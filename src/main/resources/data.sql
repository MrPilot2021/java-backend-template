-- 用户表
insert into `user_info`(`user_name`, `phone`, `password`) values
('菜根花', '18165555744','0123'),
('feh', '18810121889','123456');

-- 车次信息表
insert into `train_info` (`train_number`,`departure`,`destination`,`leave_time`,`arrival_time`) values
('G1024', '深圳','北京','2022-10-21 19:00:00','2022-10-22 19:00:00'),
('G624', '西安','北京','2022-10-22 10:00:00','2022-10-22 19:00:00');

-- 票务信息表
insert into `ticket_info` (`train_number`,`container`,`level`,`position`,`price`,`ordered`) values
('G1024', 8,'一等座','7J',80,1),
('G624', 2,'二等座','6F',40,1),
('G1024', 12,'软座','4A',130,0),
('G1024', 12,'软座','4B',130,0),
('G1024', 12,'软座','4C',130,0),
('G1024', 8,'一等座','7H',80,0),
('G624', 14,'软座','9I',130,0),
('G624', 10,'一等座','7C',80,0);

-- 订票记录表
insert into `ticket_record` (`user_id`,`ticket_id`,`order_time`) values
(1,1, '2022-10-5 10:00:00'),
(2,2, '2022-10-14 12:00:00');
