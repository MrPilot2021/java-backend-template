-- 用户信息
DROP TABLE if EXISTS user_info;
CREATE TABLE user_info
(
    user_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    user_name varchar(255) NOT NULL,
    phone varchar(20) ,
    password varchar(20) NOT NULL

    -- real_name varchar(20) NOT NULL, -- 真是姓名
    -- gender varchar(10) NOT NULL, --性别
    -- type varchar(10) NOT NULL, -- 用户种类（学生，老人，儿童，普通人？）
    -- address varchar(255) NOT NULL, -- 地址
    -- birthday varchar(20) NOT NULL, -- 生日
    -- id_number varchar(20) NOT NULL, -- 身份证号
);

-- 车辆信息 Train_Info
DROP TABLE if EXISTS train_info;
CREATE TABLE train_info
(
    train_number varchar(10) PRIMARY KEY NOT NULL, -- 车次编号
    departure varchar(20) NOT NULL, -- 始发站
    destination varchar(20) NOT NULL,
    leave_time varchar(50) NOT NULL, -- 始发时间
    arrival_time varchar(50) NOT NULL
);

-- 余票信息Ticket_Info Ticket_sub_Info
DROP TABLE if EXISTS ticket_info;
CREATE TABLE ticket_info -- 票务信息
(

    ticket_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,-- 票的id

    train_number varchar(20) not null, -- 车次

    container INTEGER not null, -- 车厢号

    level varchar(20) not null, -- 座位等级

    position varchar(3) not null, --座位

    price double not null,	-- 对应的price

    ordered INTEGER not null default 0 -- 是否被预定（为0代表未被预定，为1代表被预定）

);

DROP TABLE if EXISTS ticket_record;
CREATE TABLE ticket_record -- 订票记录
(
    user_id INTEGER not null, -- 用户编号

    ticket_id INTEGER NOT NULL, -- 票号

    order_time varchar(20) not null, -- 下单时间

    PRIMARY KEY(user_id, ticket_id)
);







