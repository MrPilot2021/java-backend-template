package com.citic.reservation.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@ControllerAdvice
/**
 * RestControllerAdvice会自动帮助catch,并匹配相应的ExceptionHandler,
 * 然后重新封装异常信息，返回值，统一格式返回给前端。
 * 并且具有ResponseBody的功能，将方法的返回值，以特定的格式写入到response的body区域，
 * 进而将数据返回给客户端。当方法上面没有写ResponseBody,底层会将方法的返回值封装为ModelAndView对象。
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    //捕获校验注解抛出的异常，并处理异常信息返回前端。
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ExceptionResponse methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        // 从异常对象中拿到ObjectError对象
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        // 然后提取错误提示信息进行返回
        String errorMsg = objectError.getDefaultMessage();
        log.error(errorMsg, e);
        return new ExceptionResponse(errorMsg);
    }

    // 捕获数据库相关异常，处理并直接返回结果至前端
    @ExceptionHandler(BizExcepton.class)
    public ExceptionResponse databaseExceptionHandler(BizExcepton e) {
        log.error(e.getErrorMsg(), e);
        return new ExceptionResponse(e.getErrorMsg());
    }

}
