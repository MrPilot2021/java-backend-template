package com.citic.reservation.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BizExcepton extends RuntimeException{

    /**
     * 错误信息
     */
    private String errorMsg;

}
