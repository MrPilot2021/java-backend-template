package com.citic.reservation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = {"com.citic.reservation.dao"})
public class TicketReservationSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketReservationSystemApplication.class, args);
    }

}
