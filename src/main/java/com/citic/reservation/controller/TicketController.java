package com.citic.reservation.controller;

import com.citic.reservation.controller.request.ListAllTicketRequest;
import com.citic.reservation.controller.request.TicketInventoryQueryRequest;
import com.citic.reservation.controller.request.TicketReservationRequest;
import com.citic.reservation.controller.response.ListAllTicketResponse;
import com.citic.reservation.controller.response.TicketInventoryQueryResponse;
import com.citic.reservation.controller.response.TicketReservationResponse;
import com.citic.reservation.service.TicketService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/project/ticket")
public class TicketController {

    @Resource
    TicketService ticketService;

    @PostMapping("/query/inventory")
    public TicketInventoryQueryResponse ticketInfoQuery(@Valid @RequestBody TicketInventoryQueryRequest request) {
        return ticketService.queryTicketInventory(request);
    }

    @PostMapping("/list")
    public ListAllTicketResponse listAllTickets(@Valid @RequestBody ListAllTicketRequest request) {
        return ticketService.listAllTickets(request);
    }

    @PostMapping("/reserve")
    public TicketReservationResponse ticketReservation(@Valid @RequestBody TicketReservationRequest request) {
        return ticketService.orderTickets(request);
    }



}
