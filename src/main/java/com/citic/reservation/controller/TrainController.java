package com.citic.reservation.controller;

import com.citic.reservation.controller.request.ListTrainInfoRequest;
import com.citic.reservation.controller.request.TrainInfoQueryRequest;
import com.citic.reservation.controller.response.ListTrainInfoResponse;
import com.citic.reservation.controller.response.TrainInfoQueryResponse;
import com.citic.reservation.service.TrainService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/project/train")
public class TrainController {

    @Resource
    TrainService trainService;

    @PostMapping("/query")
    public TrainInfoQueryResponse trainQuery(@Valid @RequestBody TrainInfoQueryRequest request) {
        return trainService.queryTrainInfo(request);
    }
    @PostMapping("/list")
    public ListTrainInfoResponse AllTrainQuery(@Valid @RequestBody ListTrainInfoRequest request) {
        return trainService.listAllTrainInfo(request);
    }

}
