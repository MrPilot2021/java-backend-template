package com.citic.reservation.controller.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainInfoQueryResponse {

    private Boolean status;
    private String description;
    private String departure;
    private String destination;
    private String departureTime;
    private String arrivalTime;

}
