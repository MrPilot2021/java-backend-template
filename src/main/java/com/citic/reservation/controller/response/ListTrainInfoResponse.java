package com.citic.reservation.controller.response;


import com.citic.reservation.dao.DO.TrainDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListTrainInfoResponse {

    private List<TrainDO> trainList;

    private Boolean Status;

    private String Description;

}
