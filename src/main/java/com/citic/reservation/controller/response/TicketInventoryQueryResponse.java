package com.citic.reservation.controller.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TicketInventoryQueryResponse {

    private String train;
    private String level;
    private Integer inventory;
}
