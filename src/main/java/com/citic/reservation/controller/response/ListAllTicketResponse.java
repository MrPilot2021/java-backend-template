package com.citic.reservation.controller.response;

import com.citic.reservation.dao.DO.InventoryDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListAllTicketResponse {

    private List<InventoryDO> ticketList;
}
