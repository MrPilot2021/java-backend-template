package com.citic.reservation.controller;

import com.citic.reservation.controller.request.ListAllUserRequest;
import com.citic.reservation.controller.request.LoginRequest;
import com.citic.reservation.controller.request.LogoutRequest;
import com.citic.reservation.controller.request.RegisterRequest;
import com.citic.reservation.controller.response.ListAllUserResponse;
import com.citic.reservation.controller.response.LoginResponse;
import com.citic.reservation.controller.response.LogoutResponse;
import com.citic.reservation.controller.response.RegisterResponse;
import com.citic.reservation.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/project/user")
public class UserController {

    @Resource
    UserService userService;

    @PostMapping("/login")
    public LoginResponse userLogin(@Valid @RequestBody LoginRequest request) {
        return userService.userLogin(request);
    }

    @PostMapping("/logout")
    public LogoutResponse userLogout(@Valid @RequestBody LogoutRequest request) {
        return userService.userLogout(request);
    }

    @PostMapping("/register")
    public RegisterResponse userLogin(@Valid @RequestBody RegisterRequest request) {
        return userService.createUser(request);
    }

    @PostMapping("/list")
    public ListAllUserResponse listAllUser(@Valid @RequestBody ListAllUserRequest request) {
        return userService.listAllUsers(request);
    }


}
