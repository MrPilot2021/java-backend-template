package com.citic.reservation.authentication;

import com.citic.reservation.exception.BizExcepton;
import com.citic.reservation.utils.LocalCacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AuthenticationIntercepter  extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 获取前端username
        String username = request.getHeader("Authorization");

        // 以防前端忘记
        if (username==null) {
            throw new BizExcepton(String.format("访问 %s 时，请前端携带用户名登录",request.getRequestURI()));
        }

        // 以username查询缓存，未登录抛异常
        if (!LocalCacheUtils.isUserExist(username)) {
            throw new BizExcepton("未登录，无权限查看该页面");
        }

        log.info("访问 {},访问者权限校验通过", request.getRequestURL());
        return true;
    }
}
