package com.citic.reservation.authentication;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器的配置类
 */

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Bean
    public AuthenticationIntercepter authenticationIntercepter() {
        return new AuthenticationIntercepter();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        /**
         * 注册拦截器
         */
        registry.addInterceptor(authenticationIntercepter())
                .excludePathPatterns("/**/login")
                .excludePathPatterns("/**/logout")
                .excludePathPatterns("/**/register")
                .addPathPatterns("/**")
        ;

    }
}
