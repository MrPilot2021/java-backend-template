package com.citic.reservation.service;

import com.citic.reservation.controller.request.ListAllUserRequest;
import com.citic.reservation.controller.request.LoginRequest;
import com.citic.reservation.controller.request.LogoutRequest;
import com.citic.reservation.controller.request.RegisterRequest;
import com.citic.reservation.controller.response.ListAllUserResponse;
import com.citic.reservation.controller.response.LoginResponse;
import com.citic.reservation.controller.response.LogoutResponse;
import com.citic.reservation.controller.response.RegisterResponse;

public interface UserService {

    LoginResponse userLogin(LoginRequest request);

    LogoutResponse userLogout(LogoutRequest request);

    RegisterResponse createUser(RegisterRequest request);

    ListAllUserResponse listAllUsers(ListAllUserRequest request);
}
