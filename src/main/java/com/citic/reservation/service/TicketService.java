package com.citic.reservation.service;

import com.citic.reservation.controller.request.ListAllTicketRequest;
import com.citic.reservation.controller.request.TicketInventoryQueryRequest;
import com.citic.reservation.controller.request.TicketReservationRequest;
import com.citic.reservation.controller.response.ListAllTicketResponse;
import com.citic.reservation.controller.response.TicketInventoryQueryResponse;
import com.citic.reservation.controller.response.TicketReservationResponse;

public interface TicketService {

    TicketInventoryQueryResponse queryTicketInventory(TicketInventoryQueryRequest request);

    TicketReservationResponse orderTickets(TicketReservationRequest request);

    ListAllTicketResponse listAllTickets(ListAllTicketRequest request);

}
