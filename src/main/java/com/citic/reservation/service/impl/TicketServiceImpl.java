package com.citic.reservation.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.citic.reservation.controller.request.ListAllTicketRequest;
import com.citic.reservation.controller.request.TicketInventoryQueryRequest;
import com.citic.reservation.controller.request.TicketReservationRequest;
import com.citic.reservation.controller.response.ListAllTicketResponse;
import com.citic.reservation.controller.response.TicketInventoryQueryResponse;
import com.citic.reservation.controller.response.TicketReservationResponse;
import com.citic.reservation.dao.DO.InventoryDO;
import com.citic.reservation.dao.DO.RecordDO;
import com.citic.reservation.dao.DO.UserDO;
import com.citic.reservation.dao.InventoryDao;
import com.citic.reservation.dao.RecordDao;
import com.citic.reservation.dao.UserDao;
import com.citic.reservation.service.TicketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Resource
    InventoryDao inventoryDao;

    @Resource
    UserDao userDao;

    @Resource
    RecordDao recordDao;

    @Override
    public TicketInventoryQueryResponse queryTicketInventory(TicketInventoryQueryRequest request) {

        // 待返回的响应对象
        TicketInventoryQueryResponse response = new TicketInventoryQueryResponse();

        // 从DB获取用户DO对象
        QueryWrapper<InventoryDO> wrapper = new QueryWrapper<>();
        wrapper.eq("train_number", request.getTrain())
                .eq("level", request.getLevel())
                .eq("ordered", 0);
        List<InventoryDO> ticketList = inventoryDao.selectList(wrapper);
        response.setInventory(ticketList.size());
        response.setLevel(request.getLevel());
        response.setTrain(request.getTrain());
        return response;
    }

    @Override
    public TicketReservationResponse orderTickets(TicketReservationRequest request) {

        // 待返回的响应对象
        TicketReservationResponse response = new TicketReservationResponse();

        // 先获取到所有可被预定的票list
        QueryWrapper<InventoryDO> wrapper = new QueryWrapper<>();
        wrapper.eq("train_number", request.getTrain())
                .eq("level", request.getLevel())
                .eq("ordered", 0);
        List<InventoryDO> ticketList = inventoryDao.selectList(wrapper);


        // 检查余量
        if (ticketList==null || ticketList.size() <= 0) {
            response.setStatus(false);
            response.setDescription("余票不足，预定失败");
            return response;
        }

        // 先根据用户名拿到id
        QueryWrapper<UserDO> userWrapper = new QueryWrapper<>();
        userWrapper.eq("user_name", request.getUsername());
        UserDO userDO = userDao.selectOne(userWrapper);
        if(userDO==null){
            response.setStatus(false);
            response.setDescription("用户不存在，预定失败");
            return response;
        }

        // 随机挑一个默认第一个
        InventoryDO inventoryDO = ticketList.get(0);
        inventoryDO.setOrdered(1);
        // 更新到ticket_info表
        QueryWrapper<InventoryDO> ticketWrapper = new QueryWrapper<>();
        ticketWrapper.eq("ticket_id", inventoryDO.getTicketId());
        inventoryDao.update(inventoryDO, ticketWrapper);
        // 维护record_info表，插入记录
        RecordDO recordDO = new RecordDO();
        recordDO.setUserId(userDO.getUserId());
        recordDO.setTicketId(inventoryDO.getTicketId());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        recordDO.setOrderTime(formatter.format(date));
        recordDao.insert(recordDO);
        // 整理，返回
        response.setStatus(true);
        response.setDescription("预订成功");
        return response;
    }

    @Override
    public ListAllTicketResponse listAllTickets(ListAllTicketRequest request) {
        List<InventoryDO> ticketList = inventoryDao.selectList(null);
        ListAllTicketResponse response = new ListAllTicketResponse();
        response.setTicketList(ticketList);
        return response;
    }
}
