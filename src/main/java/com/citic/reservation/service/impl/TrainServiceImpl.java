package com.citic.reservation.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.citic.reservation.controller.request.ListTrainInfoRequest;
import com.citic.reservation.controller.request.TrainInfoQueryRequest;
import com.citic.reservation.controller.response.ListTrainInfoResponse;
import com.citic.reservation.controller.response.TrainInfoQueryResponse;
import com.citic.reservation.dao.DO.TrainDO;
import com.citic.reservation.dao.TrainDao;
import com.citic.reservation.service.TrainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TrainServiceImpl implements TrainService {

    @Resource
    TrainDao trainDao;

    @Override
    public TrainInfoQueryResponse queryTrainInfo(TrainInfoQueryRequest request) {

        // 待返回的响应对象
        TrainInfoQueryResponse response = new TrainInfoQueryResponse();

        // 从DB获取用户DO对象
        /**
         * 一般来说，一个DO就代表数据库中的一行，利用mybatis-plus获取DO的方式也很简单：
         * 新建一个wrapper注入查询语义，作为入参带入到mybatis-plus在Dao层的方法
         * 整个过程相当于执行了SQL语句[select * from user where username = xxx]
         * 一般mybatis-plus可以干掉绝大部分简单CRUD的SQL，从而极大简化开发过程
         */
        QueryWrapper<TrainDO> wrapper = new QueryWrapper<>();
        wrapper.eq("train_number", request.getNumber());
        TrainDO trainDO = trainDao.selectOne(wrapper);

        // 如果列车不存在
        if(trainDO==null){
            response.setStatus(false);
            response.setDescription("列车不存在");
        } else{
            response.setStatus(true);
            response.setDescription("查询成功");
            response.setDeparture(trainDO.getDeparture());
            response.setDestination(trainDO.getDestination());
            response.setDepartureTime(trainDO.getDepartureTime());
            response.setArrivalTime(trainDO.getArrivalTime());
        }

        return response;
    }

    @Override
    public ListTrainInfoResponse listAllTrainInfo(ListTrainInfoRequest request) {
        // 待返回的响应对象
        ListTrainInfoResponse response = new ListTrainInfoResponse();
        QueryWrapper<TrainDO> wrapper = new QueryWrapper<>();
        List<TrainDO> ticketList = trainDao.selectList(wrapper);
        if(ticketList == null){
            response.setStatus(false);
            response.setDescription("无列车");
            return null;
        }else{
            response.setStatus(true);
            response.setDescription("以上为全部列车");
            response.setTrainList(ticketList);
            return response;
        }
    }
}
