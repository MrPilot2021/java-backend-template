package com.citic.reservation.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.citic.reservation.controller.request.ListAllUserRequest;
import com.citic.reservation.controller.request.LoginRequest;
import com.citic.reservation.controller.request.LogoutRequest;
import com.citic.reservation.controller.request.RegisterRequest;
import com.citic.reservation.controller.response.ListAllUserResponse;
import com.citic.reservation.controller.response.LoginResponse;
import com.citic.reservation.controller.response.LogoutResponse;
import com.citic.reservation.controller.response.RegisterResponse;
import com.citic.reservation.dao.DO.UserDO;
import com.citic.reservation.dao.UserDao;
import com.citic.reservation.exception.BizExcepton;
import com.citic.reservation.service.UserService;
import com.citic.reservation.utils.LocalCacheUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    UserDao userDao;

    @Override
    public LoginResponse userLogin(LoginRequest request) {
        // 待返回的响应对象
        LoginResponse response = new LoginResponse();

        // 从DB获取用户DO对象
        /**
         * 一般来说，一个DO就代表数据库中的一行，利用mybatis-plus获取DO的方式也很简单：
         * 新建一个wrapper注入查询语义，作为入参带入到mybatis-plus在Dao层的方法
         * 整个过程相当于执行了SQL语句[select * from user where username = xxx]
         * 一般mybatis-plus可以干掉绝大部分简单CRUD的SQL，从而极大简化开发过程
         */
        QueryWrapper<UserDO> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", request.getName());
        UserDO userDO = userDao.selectOne(wrapper);

        // 如果用户不存在
        if(userDO==null){
            response.setStatus(false);
            response.setDescription("用户不存在");
        }
        // 判断数据库里面的密码和前端输入密码是否相等
        else if(!request.getPassword().equals(userDO.getPassword())){
            response.setStatus(false);
            response.setDescription("密码不正确，请重新登录");
        }else{
            response.setStatus(true);
            response.setDescription(String.format("用户【%s】登陆成功",request.getName()));
            // 将已经登录的用户缓存至本地
            LocalCacheUtils.saveUser(request.getName());
        }

        return response;
    }

    @Override
    public LogoutResponse userLogout(LogoutRequest request) {

        LogoutResponse response = new LogoutResponse();
        response.setStatus(true);
        response.setDescription("登出成功");
        // 删除本地缓存
        LocalCacheUtils.deleteUser(request.getUsername());
        return response;
    }

    @Override
    public RegisterResponse createUser(RegisterRequest request) {

        // 待返回响应
        RegisterResponse response = new RegisterResponse();

        // 创建DO
        UserDO userDO = new UserDO();
        userDO.setUsername(request.getName());
        userDO.setPassword(request.getPassword());

        QueryWrapper<UserDO> wrapper = new QueryWrapper();
        wrapper.eq("user_name", request.getName());
        // 如果用户已存在，需要让前端感知到
        if (userDao.selectOne(wrapper) != null) {
            response.setStatus(false);
            response.setDescription("用户已存在，请勿重复创建");
            return response;
        }

        // 执行插入
        try {
            userDao.insert(userDO);
        }
        // 遇到异常时，抛出异常，交给全局异常处理
        catch (Exception e) {
            throw new BizExcepton("插入失败");
        }

        // 插入成功返回true
        response.setStatus(true);
        response.setDescription("插入成功");
        return response;
    }

    @Override
    public ListAllUserResponse listAllUsers(ListAllUserRequest request) {

        List<UserDO> userList = userDao.selectList(null);
        ListAllUserResponse response = new ListAllUserResponse();
        response.setUserDOList(userList);
        return response;
    }
}
