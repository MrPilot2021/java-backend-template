package com.citic.reservation.service;

import com.citic.reservation.controller.request.ListTrainInfoRequest;
import com.citic.reservation.controller.request.TrainInfoQueryRequest;
import com.citic.reservation.controller.response.ListTrainInfoResponse;
import com.citic.reservation.controller.response.TrainInfoQueryResponse;

public interface TrainService {

    TrainInfoQueryResponse queryTrainInfo(TrainInfoQueryRequest request);

    ListTrainInfoResponse listAllTrainInfo(ListTrainInfoRequest request);
}
