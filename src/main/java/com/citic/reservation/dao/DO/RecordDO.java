package com.citic.reservation.dao.DO;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ticket_record")
public class RecordDO {

    /**
     * 用户编号
     */
    private Integer userId;

    /**
     * 票号
     */
    private Integer ticketId;

    /**
     * 下单时间
     */
    private String orderTime;
}
