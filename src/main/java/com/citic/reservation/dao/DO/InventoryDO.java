package com.citic.reservation.dao.DO;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ticket_info")
public class InventoryDO {

    /**
     * 票号
     */
    @TableField("`ticket_id`")
    private Integer ticketId;

    /**
     * 车次编号
     */
    @TableField("`train_number`")
    private String train;

    /**
     * 车厢号
     */
    private Integer container;

    /**
     * 座位等级
     */
    private String level;

    /**
     * 座位位置
     */
    private String position;

    /**
     * 票价
     */
    private String price;

    /**
     * 是否被预定
     */
    private Integer ordered;

}
