package com.citic.reservation.dao.DO;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_info")
public class UserDO {

    /**
     * 用户名
     */
    @TableField("`user_name`")
    private String username;

    /**
     * 电话
     */
    private String phone;

    /**
     * 用户id
     */
    @TableField("`user_id`")
    private Integer userId;

    /**
     * 密码
     */
    private String password;

}
