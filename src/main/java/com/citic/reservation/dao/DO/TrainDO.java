package com.citic.reservation.dao.DO;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "train_info")
public class TrainDO {

    /**
     * 车次编号
     */
    @TableField("`train_number`")
    private String number;

    /**
     * 始发站
     */
    private String departure;

    /**
     * 终点站
     */
    private String destination;

    /**
     * 出发时间
     */
    @TableField("`leave_time`")
    private String departureTime;

    /**
     * 到达时间
     */
    @TableField("`arrival_time`")
    private String arrivalTime;
}
