package com.citic.reservation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citic.reservation.dao.DO.InventoryDO;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryDao extends BaseMapper<InventoryDO> {
}
