package com.citic.reservation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citic.reservation.dao.DO.TrainDO;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainDao extends BaseMapper<TrainDO> {
}
