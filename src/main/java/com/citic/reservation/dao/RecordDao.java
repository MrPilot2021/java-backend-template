package com.citic.reservation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citic.reservation.dao.DO.InventoryDO;
import com.citic.reservation.dao.DO.RecordDO;

public interface RecordDao extends BaseMapper<RecordDO> {
}
