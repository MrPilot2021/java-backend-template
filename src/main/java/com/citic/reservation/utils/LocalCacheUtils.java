package com.citic.reservation.utils;

import java.util.HashSet;
import java.util.Set;


public class LocalCacheUtils {

    private static Set<String> loginUsers = new HashSet<>();

    public static void saveUser(String user){
        loginUsers.add(user);
    }

    public static void deleteUser(String user){
        loginUsers.remove(user);
    }

    public static boolean isUserExist(String user){
        return loginUsers.contains(user);
    }

}
